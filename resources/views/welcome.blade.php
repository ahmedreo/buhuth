@extends('layouts.app')

@section('content')
<b-carousel id="carousel1"
	style="text-shadow: 1px 1px 2px #333;"
	controls
	indicators
	background="#ababab"
	:interval="4000"
	img-width="1024"
	img-height="400"
	>
	<b-carousel-slide img-src="{{asset('/img/3.jpg')}}"></b-carousel-slide>
	<b-carousel-slide img-src="{{asset('/img/2.jpg')}}"></b-carousel-slide>
	<b-carousel-slide img-src="{{asset('/img/1.jpg')}}"></b-carousel-slide>
	<b-carousel-slide>
		<img slot="img" class="d-block img-fluid w-100" width="1024" height="400" src="{{asset('/img/0.jpg')}}" alt="image slot">
	</b-carousel-slide>
</b-carousel>
<section id="search">
	<h2 class="title"><i class="fa fa-search"></i> @lang('search')</h2>
	<b-tabs >
		<b-tab title="@lang('general search')" active>
			<b-form>
				<b-form-group label="@lang('Different categories of search')">
					<b-form-select v-model="selected" class="mb-3">
						<option value="key_words">@lang('key words')</option>
						<option value="address">@lang('address')</option>
						<option value="author">@lang('author')</option>
						<option value="subject">@lang('subject')</option>
						<option value="country">@lang('country')</option>
						<option value="language">@lang('language')</option>
					</b-form-select>
				</b-form-group>
				<b-form-group label="ابحث فى قاعدة البيانات" label-for="search">
					<b-form-input id="search" type="text" placeholder="@lang('search')"></b-form-input>
				</b-form-group>
				<b-form-group>
		  			<b-button>@lang('search')</b-button>
				</b-form-group>
			</b-form>
		</b-tab>
		<b-tab title="@lang('university thesis')" >
			<br>I'm the second tab content
		</b-tab>
		<b-tab title="@lang('books')" >
			<br>I'm the second tab content
		</b-tab>
		<b-tab title="@lang('articals')" >
			<br>I'm the second tab content
		</b-tab>
	</b-tabs>
</section>
<section id="news">
	<h2 class="title"><i class="fa fa-newspaper-o"></i> @lang('news')</h2>
	<b-row>
		<b-col>
			<h5 class="mb-30">@lang('Fast links')</h5>
			<b-card>
				<p><a href="#">@lang('Recent additions')</a></p>
				<p><a href="#">@lang('Newsletter')</a></p>
			</b-card>
		</b-col>
		<b-col :cols="6">
			<h5 class="mb-30">@lang('guestbook')</h5>
			<b-card>
				<b-media no-body>
					<b-media-aside>
						<b-img class="media-pic" slot="aside" src="{{asset('img/user.png')}}" alt="placeholder" />
					</b-media-aside>
					<b-media-body class="ml-3">
						<h5 class="mt-0">د.إسلام يسري</h5>
						<p>
							كلية تربية الجامعة الاسلامية العالمية بماليزيا
						</p>
						<p class="mb-30">
							موقع به كامل التفاصيل والخبرات التربويةالتى تعنى بمصطلحات وعلوم التربية فى جميع التخصصات خاصة تعليم العربية للناطقين بغيرها 
						</p>
						<b-pagination size="sm" :total-rows="100" :per-page="10"></b-pagination>
					</b-media-body>
				</b-media>
			</b-card>
		</b-col>
		<b-col>
			<h5 class="mb-30">@lang('tweets')</h5>
			<b-card>
				
			</b-card>
		</b-col>
	</b-row>
</section>
<section id="news">
	<h2 class="title"><i class="fa fa-trophy"></i> @lang('numbers')</h2>
	<b-row>
		<b-col>
			<b-card class="text-center">
				<h2>62</h2>
				<p>@lang('scientific letter')</p>
			</b-card>
		</b-col>

		<b-col>
			<b-card class="text-center">
				<h2>41</h2>
				<p>@lang('Article in Journal')</p>
			</b-card>
		</b-col>

		<b-col>
			<b-card class="text-center">
				<h2>35</h2>
				<p>@lang('book')</p>
			</b-card>
		</b-col>

		<b-col>
			<b-card class="text-center">
				<h2>23</h2>
				<p>@lang('Conference Search')</p>
			</b-card>
		</b-col>

	</b-row>
</section>
<div id="footer">
	<b-container>
		<b-row align-h="between">
			<b-col>
				<p>@lang('call us')</p>
				<p>+202 22708415 / +2022 26706932</p>
				<p>12 - شارع نجيب محفوظ - مدينة نصر - القاهرة</p>
			</b-col>
			<b-col :cols="4">
				<p>تابعنا على :</p>
				<p>
					<a href="https://twitter.com/minimalmonkey" class="icon-button twitter"><i class="fa fa-twitter icon-twitter"></i><span></span></a>
					<a href="https://facebook.com" class="icon-button facebook"><i class="fa fa-facebook icon-facebook"></i><span></span></a>
					<a href="https://plus.google.com" class="icon-button google-plus"><i class="fa fa-google-plus icon-google-plus"></i><span></span></a>
				</p>
				<p>بحوث 2018 - جميع الحقوق محفوظه &copy;</p>
			</b-col>
		</b-row>
	</b-container>
</div >
@endsection
