<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/app.rtl.css') }}" rel="stylesheet">
</head>
<body>
	<div id="app" class="direction" v-cloak>
		<b-navbar toggleable="md" type="dark" variant="dark">
			<b-container>
				<b-navbar-toggle target="nav_collapse"></b-navbar-toggle>
				<b-navbar-brand href="{{url('/ar')}}">Buhuth | Admin</b-navbar-brand>
				<b-collapse is-nav id="nav_collapse">
					<b-navbar-nav class="ml-auto">
						@if(Auth::guest())
						<b-nav-item href="{{url('/login')}}" {{ request()->is('login') ? 'active' : ''}}>Login</b-nav-item>
						<b-nav-item href="{{url('/register')}}" {{ request()->is('register') ? 'active' : ''}}>Register</b-nav-item>
						@else
						<b-nav-item-dropdown text="{{ Auth::user()->name }}" left>
							<b-dropdown-item href="{{url('/')}}">@lang('home')</b-dropdown-item>
							<b-dropdown-item href="{{ url('/logout') }}" @click.prevent="$refs['logout-form'].submit()">@lang('logout')</b-dropdown-item>
						</b-nav-item-dropdown>
						<form ref="logout-form" action="{{ url('/logout') }}" method="POST">{{ csrf_field() }}</form>
						@endif
					</b-navbar-nav>
				</b-collapse>
			</b-container>
		</b-navbar>
		<b-container>
			@yield('content')
		</b-container>
	</div>

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
