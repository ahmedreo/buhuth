<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	@if (app()->getLocale() === 'ar')
	<link href="{{ asset('css/app.rtl.css') }}" rel="stylesheet">
	@else
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	@endif
</head>
<body>
	<div id="app" class="direction" v-cloak>
		<div id="nav">
			<b-navbar id="navbar" toggleable="md" type="light" variant="light">
				<b-container>
					<b-navbar-toggle target="nav_collapse"></b-navbar-toggle>
					<b-navbar-brand href="{{url('/' . app()->getLocale())}}"><img src="{{asset('img/logo.png')}}" alt=""></b-navbar-brand>
					<b-collapse is-nav id="nav_collapse">
						<p class="ml-auto">@lang('number of summaries') <b-badge>621</b-badge></p>
						<b-navbar-nav class="ml-auto">
							<b-nav-item-dropdown right>
								<template slot="button-content">
									<i class="fa fa-globe"></i>
								</template>
								<b-dropdown-item href="{{url('/en')}}" v-if="lang !== 'en'">EN</b-dropdown-item>
								<b-dropdown-item href="{{url('/ar')}}" v-if="lang !== 'ar'">AR</b-dropdown-item>
							</b-nav-item-dropdown>
							@if(Auth::guest())
							<b-nav-item-dropdown text="@lang('admin panel')" right>
								<b-dropdown-item href="{{url('/login')}}">@lang('login')</b-dropdown-item>
								<b-dropdown-item href="{{url('/register')}}">@lang('register')</b-dropdown-item>
							</b-nav-item-dropdown>
							@else
							<b-nav-item-dropdown text="{{ Auth::user()->name }}" right>
								<b-dropdown-item href="{{ url('/admin') }}">@lang('admin panel')</b-dropdown-item>
								<b-dropdown-item href="{{ url('/logout') }}" @click.prevent="$refs['logout-form'].submit()">@lang('logout')</b-dropdown-item>
							</b-nav-item-dropdown>
							<form ref="logout-form" action="{{ url('/logout') }}" method="POST">{{ csrf_field() }}</form>
							@endif
						</b-navbar-nav>
					</b-collapse>
				</b-container>
			</b-navbar>
			<b-nav id="sub-nav" justified pills>
				<b-container>
					<b-row>
						<b-col>
							<b-nav-item-dropdown text="@lang('who are we ?')">
								<b-dropdown-item :href="'/' + lang + '/researches'">@lang('the researches')</b-dropdown-item>
								<b-dropdown-item :href="'/' + lang + '/vision'">@lang('the vision')</b-dropdown-item>
								<b-dropdown-item :href="'/' + lang + '/founder'">@lang('word of founder')</b-dropdown-item>
							</b-nav-item-dropdown>

						</b-col>
						<b-col>
							<b-nav-item-dropdown text="@lang('add to buhuth')">
								<b-dropdown-item href="{{url(app()->getLocale().'/researches/create')}}">@lang('add research')</b-dropdown-item>
								<b-dropdown-item href="{{url(app()->getLocale().'/experts/create')}}">@lang('add expert')</b-dropdown-item>
							</b-nav-item-dropdown>

						</b-col>
						<b-col>
							<b-nav-item-dropdown text="@lang('scout')">
								<b-dropdown-item href="{{url(app()->getLocale().'/search')}}">@lang('search')</b-dropdown-item>
								<b-dropdown-item href="{{url(app()->getLocale().'/search/author')}}">@lang('browse by author')</b-dropdown-item>
								<b-dropdown-item href="{{url(app()->getLocale().'/search/subject')}}">@lang('browse by subject')</b-dropdown-item>
								<b-dropdown-item href="{{url(app()->getLocale().'/search/type_of_material')}}">@lang('browse by type of material')</b-dropdown-item>
							</b-nav-item-dropdown>

						</b-col>
						<b-col>
							<b-nav-item-dropdown text="@lang('list of honour')">
								<b-dropdown-item href="{{url(app()->getLocale().'/volunteers')}}">@lang('list of volunteers')</b-dropdown-item>
								<b-dropdown-item href="{{url(app()->getLocale().'/experts')}}">@lang('list of experts')</b-dropdown-item>
							</b-nav-item-dropdown>

						</b-col>
						<b-col>
							<b-nav-item-dropdown text="@lang('join us')">
								<b-dropdown-item href="{{url(app()->getLocale().'/login')}}">@lang('login')</b-dropdown-item>
								<b-dropdown-item href="{{url(app()->getLocale().'/guestbook')}}">@lang('type in guestbook')</b-dropdown-item>
							</b-nav-item-dropdown>

						</b-col>
						<b-col>
							<b-nav-item-dropdown text="@lang('call us')">
								<b-dropdown-item href="{{url(app()->getLocale().'/questions')}}">@lang('common questions')</b-dropdown-item>
								<b-dropdown-item href="{{url(app()->getLocale().'/questions/create')}}">@lang('send to us')</b-dropdown-item>
							</b-nav-item-dropdown>

						</b-col>
					</b-row>
				</b-container>
			</b-nav>
		</div>
		
		<b-container>
			@yield('content')
		</b-container>
		
	</div>

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
