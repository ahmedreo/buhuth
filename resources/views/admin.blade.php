@extends('layouts.admin')

@section('content')
<b-row class="mt-30">
	<b-col>
		<b-tabs>
			<b-tab title="مراجعة البحوث" active>
				<research-admin></research-admin>
			</b-tab>
			<b-tab title="رفع ملفات">
				<research-upload></research-upload>
			</b-tab>
		</b-tabs>
	</b-col>
</b-row>
@endsection
	