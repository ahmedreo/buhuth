@extends('layouts.app')

@section('content')

@if(session('success-message'))
	<b-row class="mt-30">
		<b-col>
			<b-alert variant="success" dismissible show>
				{{session('success-message')}}
			</b-alert>
		</b-col>
	</b-row>
@endif
<b-row class="mt-30">
	<b-col>
		<b-breadcrumb>
			<b-breadcrumb-item text="@lang('who are we ?')"></b-breadcrumb-item>
			<b-breadcrumb-item text="@lang('the researches')"></b-breadcrumb-item>
		</b-breadcrumb>		
	</b-col>
</b-row>
<b-row class="mt-30">
	<b-col>
		<b-card no-body>
			<b-tabs pills card vertical>
				<b-tab title="@lang("Master's and doctoral dissertations")">
					Tab Contents 1
				</b-tab>
				<b-tab title="@lang('Articles in scientific journals')" active>
					<research-home></research-home>
				</b-tab>
				<b-tab title="@lang('Conference Papers')">
					Tab Contents 2
				</b-tab>
				<b-tab title="@lang('Academic books / educational books')">
					Tab Contents 2
				</b-tab>
				<b-tab title="@lang('Book Reviews')">
					Tab Contents 2
				</b-tab>
				<b-tab title="@lang('Classes in academic books')">
					Tab Contents 2
				</b-tab>
				<b-tab title="@lang('Reports on conferences or seminars')">
					Tab Contents 2
				</b-tab>
			</b-tabs>
		</b-card>
	</b-col>
</b-row>
@endsection