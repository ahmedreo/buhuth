@extends('layouts.admin')

@section('content')
<b-container>
    <b-row class="top-space" align-h="center">
        <b-col cols="8" >
            <b-card header="Login" border-variant="light">
                <b-form method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <b-form-group label="E-Mail Address">
                        <b-form-input type="email" name="email" value="{{ old('email') }}" state="{{$errors->has('email') ? 'invalid':null}}"></b-form-input>
                        <b-form-feedback>{{ $errors->first('email') }}</b-form-feedback>
                    </b-form-group>
                    <b-form-group label="Password">
                        <b-form-input type="password" name="password" state="{{$errors->has('password') ? 'invalid' : null}}"></b-form-input>
                        <b-form-feedback>{{ $errors->first('password') }}</b-form-feedback>
                    </b-form-group>
                    <b-button type="submit" variant="primary">Login</b-button>
                    <!-- <a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a> -->
                </b-form>
            </b-card>

        </b-col>
    </b-row>
</b-container>
@endsection

