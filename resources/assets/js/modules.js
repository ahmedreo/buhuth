import BootstrapVue from 'bootstrap-vue';
import VueInternalization from 'vue-i18n';
import Locales from './vue-i18n-locales.generated.js';
import VeeValidate from 'vee-validate';

Vue.use(BootstrapVue);
Vue.use(VueInternalization);
Vue.use(VeeValidate);

Vue.config.lang = document.documentElement.lang;
Object.keys(Locales).forEach(function (lang) {
  Vue.locale(lang, Locales[lang])
});

Vue.config.productionTip = false;
Vue.config.devtools = true;
