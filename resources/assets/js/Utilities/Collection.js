export default class Collection {
	constructor (data = []){
		this.data = this.recursive(data)
		this.limit = Infinity
		this.setUniqueID();
	}

	recursive(array){
		array.forEach(item => {
			for(let value in item){
				if (item[value] instanceof Array) {
					this.recursive(item[value])
					item[value] = new Collection(item[value]);
				}
			}
		})
		return array;
	}

	setUniqueID(){
		this.data.forEach( (record , index) => {
			record.id = index;
		})
	}	
	/*
	* get() used to retrieve data in the collection
	* notice that if you have nested collections use all instead of get()
	*/
	
	get(prop = null){
		if (prop) {	
			if (this.data.some(item => item.hasOwnProperty(prop) ) ) {
				return this.data.map(item => item[prop]);
			}
			else{
				return null
			}
		}
		return this.data
	}
	
	/**
	* first() used to retrieve first item in the collection
	*/
	
	first(){
		return this.data[0]
	}
	
	/**
	* count() used to fetch the number of items inside the collection
	*/
	
	count(){
		return this.data.length
	}
	
	/**
	* last() used to retrieve the last item in the collection
	*/
	
	last(){
		return this.data[this.count() - 1]
	}
	
	reset(obj){
		for (var key in obj){
			if (obj.hasOwnProperty(key)){
				if (typeof obj[key] === 'string'){
					obj[key] = undefined;
				} 
				else if (obj[key] instanceof Array){
					obj[key] = [];
				}
				else if (obj[key] instanceof Collection) {
					obj[key]['data'].forEach(record => {
						this.reset(record)
					})
				}
			}
		}
		return obj
	}
	
	getInstance(){
		let instance = _.cloneDeep(this.last());
		return this.reset(instance)
	}
	
	/**
	* new() used to push new empty instance 
	*/
	
	new(){
		if (this.count() < this.limit) {
			this.data.push(this.getInstance());
			console.log(this.data);
			this.setUniqueID();
		}
		else{
			console.error('This collection have limitation to ' + this.limit);	
		}
		
	}
	
	/**
	* pop() used to remove last item in the collection but in case 
	* you have just one item this method will empty it .
	*/
	
	pop(){
		if (this.data.length > 1) {
			this.data.pop();
		}
		else{
			this.reset(this.first());
		}
	}
	
	/**
	* if you want to convert a parent collection and all its nested collection
	* to Arrays simply use method all()	.		
	*/
	
	all(){
		let instance = _.cloneDeep(this.data);
		return this.toArray(instance)
	}
	
	toArray(instance){
		instance.forEach(item => {
			for(let value in item){
				if (item[value] instanceof Collection) {
					this.toArray(item[value]['data'])
					item[value] = item[value]['data'];
				}
			}
		})
		return instance
	}
	limitTo(n){
		this.limit = n
		return true;
	}
	take(n){
		return this.data.slice(0 , n);
	}
}