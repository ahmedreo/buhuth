
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
require('./modules');
require('./globals');
require('./ValidationMessages');
require('./mixins');
require('./components');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.Bus = new Vue() 

const app = new Vue({
    el: '#app',
    data:{
    	lang : document.documentElement.lang,
    	selected: "author"
    }
});
