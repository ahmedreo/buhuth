import { Validator } from 'vee-validate';

const dictionary = {
  en: {
    messages:{
      required: () => 'Required'
    }
  },
  ar: {
    messages: {
      required: () => 'هذا الحقل مطلوب',
      max_value:(f ,p ,d) => 'الرقم يجب ان يكون اقل من ' + p,
      min_value:(f ,p ,d) => 'الرقم يجب ان يكون أكثر من ' + p,
      after:(f,p,d) => 'هذا الحقل يجب انا يكون اكبر من الحقل السابق'
    }
  }
};

Validator.localize(dictionary);

Validator.localize(Vue.config.lang);