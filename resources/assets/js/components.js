Vue.component('CountriesArabic', require('./countries/ar.vue'));

// Research
Vue.component('ResearchAdmin', require('./components/Research/ResearchAdmin.vue'));
Vue.component('ResearchHome', require('./components/Research/ResearchHome.vue'));
Vue.component('ResearchUpload', require('./components/Research/ResearchUpload.vue'));
Vue.component('ResearchCreate', require('./components/Research/ResearchCreate.vue'));
Vue.component('ResearchUpdate', require('./components/Research/ResearchUpdate.vue'));

// Article 
Vue.component('article-index', require('./components/Article/Index.vue'));
Vue.component('article-create', require('./components/Article/Create.vue'));
Vue.component('article-update', require('./components/Article/Update.vue'));

// Book 
Vue.component('book-index', require('./components/Book/Index.vue'));
Vue.component('book-create', require('./components/Book/Create.vue'));
Vue.component('book-update', require('./components/Book/Update.vue'));

// Review 
Vue.component('review-index', require('./components/Review/Index.vue'));
Vue.component('review-create', require('./components/Review/Create.vue'));
Vue.component('review-update', require('./components/Review/Update.vue'));

// Report 
Vue.component('report-index', require('./components/Report/Index.vue'));
Vue.component('report-create', require('./components/Report/Create.vue'));
Vue.component('report-update', require('./components/Report/Update.vue'));

// Paper 
Vue.component('paper-index', require('./components/Paper/Index.vue'));
Vue.component('paper-create', require('./components/Paper/Create.vue'));
Vue.component('paper-update', require('./components/Paper/Update.vue'));

// Class 
Vue.component('class-index', require('./components/Class/Index.vue'));
Vue.component('class-create', require('./components/Class/Create.vue'));
Vue.component('class-update', require('./components/Class/Update.vue'));

// Dissertation 
Vue.component('dissertation-index', require('./components/Dissertation/Index.vue'));
Vue.component('dissertation-create', require('./components/Dissertation/Create.vue'));
Vue.component('dissertation-update', require('./components/Dissertation/Update.vue'));
