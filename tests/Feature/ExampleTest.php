<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    public function getAdmin()
    {
        return \App\User::where('email' , 'admin@admin.com')->first();
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->actingAs($this->getAdmin())
                        ->get('/api/admin/researches');

        dump($response->getContent());
        $response->assertStatus(200);

    }
}
