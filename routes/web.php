<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function(){
	return response()->Json(['researches' => App\Research::get()]);
});

Auth::routes();

Route::get('/admin', 'AdminController@index')->name('admin');

Route::namespace('Api')->prefix('api')->group(function(){
	Route::namespace('Admin')->prefix('admin')->group(function(){
		Route::resources(['researches' => 'ResearchController']);
		Route::post('researches/approve' , 'ResearchController@approve');
		Route::post('researches/reset' , 'ResearchController@reset');
	});
	Route::resources(['categories' => 'CategoryController']);
	Route::resources(['main_subjects' => 'MainSubjectController']);
	Route::resources(['researches' => 'ResearchController']);
});


Route::get('/get_file' , 'StorageController@getFile');


Route::get('/' , function () {
	return redirect('/ar');
});


Route::middleware('language')->prefix('/{locale}')->group(function(){	
	Route::view('/researches' , 'researches');
	Route::view('/researches/create' , 'researches-create');
	Route::get('/' , function(){
		return view('welcome');
	});
});
