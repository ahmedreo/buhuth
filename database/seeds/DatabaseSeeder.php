<?php

use App\MainSubject;
use App\SubSubject;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	MainSubject::truncate();
    	SubSubject::truncate();
    	$this->call('SubjectsTableSeeder');
    }
}
