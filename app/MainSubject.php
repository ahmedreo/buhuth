<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MainSubject extends Eloquent
{
    protected $fillable = ['title'];
    
    protected $with = ['subSubjects'];

    public function subSubjects()
    {
    	return $this->hasMany('App\SubSubject');
    }
    public function another()
    {
    	return $this->hasMany('App\SubSubject');
    }
}
