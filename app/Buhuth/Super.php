<?php

namespace App\Buhuth;

class Super
{
	public static function syncHasMany($relation , $ids)
	{
		$diff = $relation->pluck('_id')->diff($ids)->toArray();
		$class_name = get_class($relation->getModel());
		$class = new $class_name;
		$class->destroy($diff);
		return true;
	}
} 
