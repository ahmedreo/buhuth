<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SubSubject extends Eloquent
{
    protected $fillable = ['title'];

    public function mainSubject()
    {
    	return $this->belongsTo('App\MainSubject');
    }
}
