<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Author extends Eloquent
{
    protected $fillable = [
        'title', 'full_name', 'last_name',
    ];

    public function researches()
	{
		return $this->hasMany('App\Research');
	}
}
