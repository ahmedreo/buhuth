<?php

namespace App\Http\Controllers;

use App\Author;
use App\Category;
use App\Publisher;
use App\Research;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StorageController extends Controller
{
	public function getFile(Request $request)
	{
		$path = storage_path('app/' . $request->url);
		return response()->file($path);
	}

}
