<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\MainSubject;
use App\SubSubject;
use Illuminate\Http\Request;

class MainSubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->Json([
            'main_subjects' => MainSubject::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MainSubject  $mainSubject
     * @return \Illuminate\Http\Response
     */
    public function show(MainSubject $mainSubject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MainSubject  $mainSubject
     * @return \Illuminate\Http\Response
     */
    public function edit(MainSubject $mainSubject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MainSubject  $mainSubject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainSubject $mainSubject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MainSubject  $mainSubject
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainSubject $mainSubject)
    {
        //
    }
}
