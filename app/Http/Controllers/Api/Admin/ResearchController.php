<?php

namespace App\Http\Controllers\Api\Admin;

use App\Author;
use App\Buhuth\Super;
use App\Category;
use App\Http\Controllers\Controller;
use App\MainSubject;
use App\Publisher;
use App\Research;
use App\SubSubject;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ResearchController extends Controller
{
    public function getData()
    {
        return Research::orderBy('created_at', 'desc')->paginate(5);
    }
    public function getArticleInputs($inputs)
    {
        return [
            'type_' => $inputs->type_,
            'type' => $inputs->type,
            'language' => $inputs->language,
            'article_title_arabic' => $inputs->article_title_arabic,
            'article_title_foreign' => $inputs->article_title_foreign,
            'journal_title_arabic' => $inputs->journal_title_arabic,
            'journal_title_foreign' => $inputs->journal_title_foreign,
            'start_page' => $inputs->start_page,
            'end_page' => $inputs->end_page,
            'issn' => $inputs->issn,
            'summry_arabic' => $inputs->summry_arabic,
            'summry_foreign' => $inputs->summry_foreign,
            'journal_frequency' => $inputs->journal_frequency,
            'journal_type' => $inputs->journal_type,
            'journal_website' => $inputs->journal_website,
            'folder_number' => $inputs->folder_number,
            'folder_section' => $inputs->folder_section,
            'publishment_info' => $inputs->publishment_info,
            'publishment_country' => $inputs->publishment_country,
            'publishment_city' => $inputs->publishment_city,
            'publishment_date' => $inputs->publishment_date,
            'sender_name' => $inputs->sender_name,
            'sender_email' => $inputs->sender_email,
            'owner_email' => $inputs->owner_email,
            'owner_website' => $inputs->owner_website,
            'notes' => $inputs->notes,
            'source_url' => $inputs->source_url,
            'state' => 'wating'
        ];
    }
    public function getBookInputs($inputs)
    {
        return [
            'type_' => $inputs->type_,
            'type' => $inputs->type,
            'language' => $inputs->language,
            'book_title_arabic' => $inputs->book_title_arabic,
            'book_title_foreign' => $inputs->book_title_foreign,
            'pages_number' => $inputs->pages_number,
            'series' => $inputs->series,
            'issn' => $inputs->issn,
            'publishment_edition' => $inputs->publishment_edition,
            'publishment_info' => $inputs->publishment_info,
            'publishment_country' => $inputs->publishment_country,
            'publishment_city' => $inputs->publishment_city,
            'publishment_date' => $inputs->publishment_date,
            'sender_name' => $inputs->sender_name,
            'sender_email' => $inputs->sender_email,
            'notes' => $inputs->notes,
            'source_url' => $inputs->source_url,
            'state' => 'wating'
        ];
    }
    public function getReviewInputs($inputs)
    {
        return [
            'type_' => $inputs->type_,
            'type' => $inputs->type,
            'language' => $inputs->language,
            'review_title_arabic' => $inputs->review_title_arabic,
            'review_title_foreign' => $inputs->review_title_foreign,
            'review_website' => $inputs->review_website,
            'book_review' => $inputs->book_review,
            'start_page' => $inputs->start_page,
            'end_page' => $inputs->end_page,
            'series' => $inputs->series,
            'publishment_mean' => $inputs->publishment_mean,
            'publishment_info' => $inputs->publishment_info,
            'publishment_country' => $inputs->publishment_country,
            'publishment_city' => $inputs->publishment_city,
            'publishment_date' => $inputs->publishment_date,
            'summry_arabic' => $inputs->summry_arabic,
            'summry_foreign' => $inputs->summry_foreign,
            'sender_name' => $inputs->sender_name,
            'sender_email' => $inputs->sender_email,
            'notes' => $inputs->notes,
            'source_url' => $inputs->source_url,
            'issn' => $inputs->issn,
            'state' => 'wating'
        ];
    }
    public function createArticle($author , $row)
    {
        return $author->researches()->create([
            'type_' => $row->get('type'),
            'type' => $row->get('noaa_alothyk'),
            'language' => $row->get('lgh_alothyk'),
            'article_title_arabic' => $row->get('aanoan_almkal_balaarby'),
            'article_title_foreign' => $row->get('aanoan_almkal_blgh_akhr'),
            'journal_title_arabic' => $row->get('aanoan_aldory_balaarby'),
            'journal_title_foreign' => $row->get('aanoan_aldory_blgh_akhr'),
            'start_page' => $row->get('bday'),
            'end_page' => $row->get('nhay'),
            'issn' => $row->get('issn'),
            'summry_arabic' => $row->get('almlkhs_balaarby'),
            'summry_foreign' => $row->get('almlkhs_blgh_akhr'),
            'journal_frequency' => $row->get('otyr_aldory_shhr'),
            'journal_type' => $row->get('noaa_aldory'),
            'journal_website' => $row->get('sfh_aldory_aal_alentrnt'),
            'folder_number' => $row->get('alaadd'),
            'folder_section' => $row->get('aljz'),
            'publishment_organization' => $row->get('alnashr'),
            'publishment_country' => $row->get('aldol'),
            'publishment_city' => $row->get('almdyn'),
            'publishment_date' => $row->get('alshhr') . '/' .$row->get('alsn'),
            'sender_name' => $row->get('asm_mrsl_almaalomat'),
            'sender_email' => $row->get('brydh_alalktron'),
            'owner_email' => $row->get('albryd_alalktron_lsahb_albhth'),
            'owner_website' => $row->get('almokaa_alalktron_lsahb_albhth'),
            'notes' => $row->get('mlahthat'),
            'source_url' => $row->get('almsdr_alth_akhtht_mnh_almaalom'),
            'state' => 'wating'
        ]);
    }

    public function createBook($author , $row)
    {
        return $author->researches()->create([
            'type_' => $row->get('type'),
            'type' => $row->get('noaa_alothyk'),
            'language' => $row->get('lgh_alothyk'),
            'book_title_arabic' => $row->get('aanoan_alktab_balaarby'),
            'book_title_foreign' => $row->get('aanoan_alktab_blgh_akhr'),
            'pages_number' => $row->get('aadd_sfhat_alktab'),
            'series' => $row->get('alslsl_an_ojdt_rkm_alktab_f_alslsl'),
            'issn' => $row->get('isbn'),
            'publishment_edition' => $row->get('altbaa'),
            'publishment_info' => $row->get('alnashr'),
            'publishment_country' => $row->get('aldol'),
            'publishment_city' => $row->get('almdyn'),
            'publishment_date' => $row->get('alshhr') . '/' .$row->get('alsn'),
            'sender_name' => $row->get('asm_mrsl_almaalomat'),
            'sender_email' => $row->get('brydh_alalktron'),
            'notes' => $row->get('mlahthat'),
            'source_url' => $row->get('almsdr_alth_akhtht_mnh_almaalom'),
            'state' => 'wating'
        ]);
    }
    public function createReview($author , $row)
    {
        return $author->researches()->create([
            'type_' => $row->get('type'),
            'type' => $row->get('noaa_alothyk'),
            'language' => $row->get('lgh_alothyk'),
            'review_title_arabic' => $row->get('aanoan_alktab_alth_tmt_mrajaath_balaarby'),
            'review_title_foreign' => $row->get('aanoan_alktab_blgh_akhr'),
            'review_website' => $row->get('alrabt_aal_alantrnt_an_ojd'),
            'book_review' => $row->get('sfhat_mrajaa_alktab'),
            'start_page' => $row->get('mn'),
            'end_page' => $row->get('al'),
            'series' => $row->get('alslsl_an_ojdt_rkm_alktab_f_alslsl'),
            'issn' => $row->get('isbn'),
            'publishment_mean' => $row->get('asm_osyl_alnshr_mjl_mokaa'),
            'publishment_info' => $row->get('alnashr'),
            'publishment_country' => $row->get('aldol'),
            'publishment_city' => $row->get('almdyn'),
            'publishment_date' => $row->get('alshhr') . '/' .$row->get('alsn'),
            'summry_arabic' => $row->get('almlkhs_balaarby'),
            'summry_foreign' => $row->get('almlkhs_blgh_akhr'),
            'sender_name' => $row->get('asm_mrsl_almaalomat'),
            'sender_email' => $row->get('brydh_alalktron'),
            'notes' => $row->get('mlahthat'),
            'source_url' => $row->get('almsdr_alth_akhtht_mnh_almaalom'),
            'state' => 'wating'
        ]);
    }
    public function separateModels($models)
    {
        $collection = collect($models);
        return array(
            'ids' => $collection->pluck('_id')->all(),
            'attributes' => $collection->where('_id' , null)->all()
        );
    }
    public function updateCategories($categories , $research)
    {
        if (count($categories) > 0) {
            $research->categories()->sync($this->separateModels($categories)['ids']);
            foreach ($this->separateModels($categories)['attributes'] as $value) {
                $research->categories()->firstOrCreate(['title' => $value['title']]);
            }
        }     
        return;
    }
    public function updateCoAuthors($co_authors , $research)
    {   
        $co_authors = collect($co_authors)->recursive();
        foreach ($co_authors as $co_author) {
            if ($co_author->has('_id')) {
                Author::find($co_author->get('_id'))->update([
                    'full_name' => $co_author->get('full_name'),
                    'last_name' => $co_author->get('last_name')
                ]);
            }
            else{
                $research->coAuthors()->create([
                    'full_name' => $co_author->get('full_name'),
                    'last_name' => $co_author->get('last_name')
                ]);
            }
        }
        return;
    }
    public function updatePublishers($publishers , $research)
    {   
        $publishers = collect($publishers)->recursive();
        foreach ($publishers as $publisher) {
            if ($publisher->has('_id')) {
                Publisher::find($publisher->get('_id'))->update([
                    'title' => $publisher->get('title'),
                    'full_name' => $publisher->get('full_name'),
                    'last_name' => $publisher->get('last_name')
                ]);
            }
            else{
                $research->coAuthors()->create([
                    'title' => $publisher->get('title'),
                    'full_name' => $publisher->get('full_name'),
                    'last_name' => $publisher->get('last_name')
                ]);
            }
        }
        return;
    }
    public function updateAuthor($author)
    {
        $author = collect($author)->recursive();
        Author::find($author->get('_id'))->update([
            'title' => $author->get('title'),
            'full_name' => $author->get('full_name'),
            'last_name' => $author->get('last_name')
        ]);
        return true;
    }
    public function updateSubjects($subjects , $research)
    {
        $subjects = collect($subjects);               
        
        $research->mainSubjects()->sync($subjects->pluck('_id')->all());
        
        $research->subSubjects()->sync($subjects->pluck('sub_subjects')->collapse()->pluck('_id')->all());
       
        return true;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->Json(['researches' => $this->getData()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Excel::load($request->excel , function($reader){
            $reader->each(function($sheet) {
                // ($sheet)->dd();
                $sheet->each(function($row) {
                    if($row->get('type')){

                        $author = Author::create([
                                'title' => $row->get('allkb'),
                                'full_name' => $row->get('alasm_alkaml_llmoelf'),
                                'last_name' => $row->get('asm_alaaael_llmoelf'),
                            ]);

                        if ($row->get('alasm_alkaml_llmoelf_almshark')) {
                            $co_author_id = Author::create([
                                'full_name' => $row->get('alasm_alkaml_llmoelf_almshark'),
                                'last_name' => $row->get('asm_alaaael_llmoelf_almshark'),
                                ])->id;
                        }
                        if ($row->get('alasm_balkaml_llmhrr')) {
                            $publisher_id = Publisher::create([
                                'full_name' => $row->get('alasm_balkaml_llmhrr'),
                                'last_name' => $row->get('asm_alaaael_llmhrr')
                                ])->id;
                        }
                        if ($row->get('alasm_balkaml_llmhrr_almdaf')) {
                            $co_publisher_id = Publisher::create([
                                'full_name' => $row->get('alasm_balkaml_llmhrr_almdaf'),
                                'last_name' => $row->get('asm_alaaael_llmhrr_almdaf')
                                ])->id;
                        }
                        $key_words = preg_split('/,|،|-|–|\t|\r/', $row->get('alklmat_almftahy'));
                        for ($i=0; $i < count($key_words) ; $i++) { 
                            if ($key_words[$i]) {
                                Category::firstOrCreate(['title' => $key_words[$i] ]);
                            }
                        }
                        
                        $category_ids = Category::whereIn('title' , $key_words)->get()->pluck('id')->toArray();

                        if ($row->get('type') === 'article') {
                            $research = $this->createArticle($author , $row);
                        }
                        elseif ($row->get('type') === 'book') {
                            $research = $this->createBook($author , $row);
                        }
                        elseif ($row->get('type') === 'review') {
                            $research = $this->createReview($author , $row);
                        }
                        if (isset($co_author_id)) {
                            $research->coAuthors()->attach($co_author_id);
                        }
                        if (isset($publisher_id)) {
                            $research->publishers()->attach([$publisher_id]);
                        }
                        if (isset($co_publisher_id)) {
                            $research->publishers()->attach([$co_publisher_id]);
                        }
                        if (isset($category_ids)) {
                            $research->categories()->attach($category_ids);
                        }
                    }
                });
            });
        });
        return response()->Json(['state' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function show(Research $research)
    {
        return response()->json(['research' => $research]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function edit(Research $research)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Research $research)
    {
        $this->updateCategories($request->categories , $research);
        $this->updateAuthor($request->author);
        $this->updateCoAuthors($request->co_authors , $research);
        $this->updatePublishers($request->publishers , $research);
        $this->updateSubjects($request->mainSubjectsAll , $research);

        if ($request->type_ === "article") {
            $research->update($this->getArticleInputs($request));
        }
        elseif ($request->type_ === "book") {
            $research->update($this->getBookInputs($request));
        }
        elseif ($request->type_ === "review") {
            $research->update($this->getReviewInputs($request));
        }
        return response()->Json(['researches' => $this->getData()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function destroy(Research $research)
    {
        $research->delete();
        return response()->Json(['researches' => $this->getData()]);
    }
    public function approve(Request $request)
    {        
        $research = Research::find($request->id);
        $research->state = 'approved';
        $research->save();    
        return response()->Json(['researches' => $this->getData()]);
    }
    public function reset(Request $request)
    {        
        $research = Research::find($request->id);
        $research->state = 'wating';
        $research->save();    
        return response()->Json(['researches' => $this->getData()]);
    }
}
