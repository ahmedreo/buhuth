<?php

namespace App\Http\Controllers\Api;

use App\Author;
use App\Category;
use App\Http\Controllers\Controller;
use App\MainSubject;
use App\Publisher;
use App\Research;
use Illuminate\Http\Request;

class ResearchController extends Controller
{
    public function getData()
    {
        return Research::where('state' , 'approved')->orderBy('created_at', 'desc')->paginate(5);
    }
    public function createSubjects($inputs , $research)
    {
        $main_subjects = collect($inputs->main_subjects);
        foreach ( $main_subjects as $main_subject) {
            if(isset($main_subject->_id)){
                $model = MainSubject::where('_id' , $main_subject->_id)->first();
                $research->mainSubjects()->attach($main_subject->_id);
                foreach ($main_subject->sub_subjects as $sub) {
                    $research->subSubjects()->attach($sub->_id);
                }           
            }
        }
        return true;
    }
    public function createArticle($author , $inputs)
    {
        return $author->researches()->create([
            'type_' => $inputs->type_,
            'type' => $inputs->type,
            'language' => $inputs->language,
            'article_title_arabic' => $inputs->article_title_arabic,
            'article_title_foreign' => $inputs->article_title_foreign,
            'journal_title_arabic' => $inputs->journal_title_arabic,
            'journal_title_foreign' => $inputs->journal_title_foreign,
            'start_page' => $inputs->start_page,
            'end_page' => $inputs->end_page,
            'issn' => $inputs->issn,
            'summry_arabic' => $inputs->summry_arabic,
            'summry_foreign' => $inputs->summry_foreign,
            'journal_frequency' => $inputs->journal_frequency,
            'journal_type' => $inputs->journal_type,
            'journal_website' => $inputs->journal_website,
            'folder_number' => $inputs->folder_number,
            'folder_section' => $inputs->folder_section,
            'publishment_info' => $inputs->publishment_info,
            'publishment_country' => $inputs->publishment_country,
            'publishment_city' => $inputs->publishment_city,
            'publishment_date' => $inputs->publishment_date,
            'sender_name' => $inputs->sender_name,
            'sender_email' => $inputs->sender_email,
            'owner_email' => $inputs->owner_email,
            'owner_website' => $inputs->owner_website,
            'notes' => $inputs->notes,
            'source_url' => $inputs->source_url,
            'state' => 'wating'
        ]);
    }

    public function createBook($author , $inputs)
    {
        return $author->researches()->create([
            'type_' => $inputs->type_,
            'type' => $inputs->type,
            'language' => $inputs->language,
            'book_title_arabic' => $inputs->book_title_arabic,
            'book_title_foreign' => $inputs->book_title_foreign,
            'pages_number' => $inputs->pages_number,
            'series' => $inputs->series,
            'issn' => $inputs->issn,
            'publishment_edition' => $inputs->publishment_edition,
            'publishment_info' => $inputs->publishment_info,
            'publishment_country' => $inputs->publishment_country,
            'publishment_city' => $inputs->publishment_city,
            'publishment_date' => $inputs->publishment_date,
            'sender_name' => $inputs->sender_name,
            'sender_email' => $inputs->sender_email,
            'notes' => $inputs->notes,
            'source_url' => $inputs->source_url,
            'state' => 'wating'
        ]);
    }
    public function createReview($author , $inputs)
    {
        return $author->researches()->create([
            'type_' => $inputs->type_,
            'type' => $inputs->type,
            'language' => $inputs->language,
            'review_title_arabic' => $inputs->review_title_arabic,
            'review_title_foreign' => $inputs->review_title_foreign,
            'review_website' => $inputs->review_website,
            'book_review' => $inputs->book_review,
            'start_page' => $inputs->start_page,
            'end_page' => $inputs->end_page,
            'series' => $inputs->series,
            'publishment_mean' => $inputs->publishment_mean,
            'publishment_info' => $inputs->publishment_info,
            'publishment_country' => $inputs->publishment_country,
            'publishment_city' => $inputs->publishment_city,
            'publishment_date' => $inputs->publishment_date,
            'summry_arabic' => $inputs->summry_arabic,
            'summry_foreign' => $inputs->summry_foreign,
            'sender_name' => $inputs->sender_name,
            'sender_email' => $inputs->sender_email,
            'notes' => $inputs->notes,
            'source_url' => $inputs->source_url,
            'issn' => $inputs->issn,
            'state' => 'wating'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->Json(['researches' => $this->getData()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = json_decode($request->body);

        $author = Author::create([
            'title' => $inputs->author_title,
            'full_name' => $inputs->author_full_name,
            'last_name' => $inputs->author_last_name,
        ]);


        $co_author_ids = [];

        if (isset($inputs->co_author_full_name)) {
            for ($i=0; $i < count($inputs->co_author_full_name) ; $i++) { 
                
                $id = Author::create([
                    'full_name' => $inputs->co_author_full_name[$i],
                    'last_name' => $inputs->co_author_last_name[$i],
                ])->id;

                array_push($co_author_ids, $id);
            }
        }
        
        $publisher_ids = [];

        if (isset($inputs->publisher_full_name)) {
            
            for ($i=0; $i < count($inputs->publisher_full_name) ; $i++) { 
                
                $id = Publisher::create([
                    'title' => $inputs->publisher_title[$i],
                    'full_name' => $inputs->publisher_full_name[$i],
                    'last_name' => $inputs->publisher_last_name[$i],
                ])->id;

                array_push($publisher_ids, $id);
            }
        }
        

        for ($i=0; $i < count($inputs->key_words); $i++) { 
            
            Category::firstOrCreate(['title' =>$inputs->key_words[$i] ]);
        }

        $category_ids = Category::whereIn('title' , $inputs->key_words)->get()->pluck('id')->toArray();

        if ($inputs->type_ === 'article') {
            $research = $this->createArticle($author , $inputs);
        }
        elseif ($inputs->type_ === 'book') {
            $research = $this->createBook($author , $inputs);
        }
        elseif ($inputs->type_ === 'review') {
            $research = $this->createReview($author , $inputs);
        }

        $this->createSubjects($inputs , $research);

        $research->coAuthors()->attach($co_author_ids);

        $research->publishers()->attach($publisher_ids);

        $research->categories()->attach($category_ids);
        
        if ($request->hasFile('summry_file')) {
            
            $research->summry_file = $request->summry_file->store('researches/summry_files');

            $research->save();
        }

        if ($request->hasFile('full_text')) {
            
            $research->full_text = $request->full_text->store('researches/full_texts');

            $research->save();
        }

        $request->session()->flash('success-message'  , 'شكرا على تعاونكم معنا ، سنراجع بيانات البحث ثم ننشره في حالة موافقته لشروط الموقع');

        return response()->Json(['state' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function show(Research $research)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function edit(Research $research)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Research $research)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function destroy(Research $research)
    {
        //
    }
}
