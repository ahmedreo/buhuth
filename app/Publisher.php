<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Publisher extends Eloquent
{
    protected $fillable = [
        'title', 'full_name', 'last_name',
    ];
}
