<?php

namespace App;

use App\MainSubject;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Research extends Eloquent
{
	protected $guarded = [];

	protected $with = ['author' , 'publishers' , 'coAuthors' , 'categories'];

	protected $appends = ['mainSubjectsAll'];

	public function author()
	{
		return $this->belongsTo('App\Author' , 'author_id');
	}

	public function coAuthors()
	{
		return $this->belongsToMany('App\Author' , null , 'research_id' , 'co_author_id');
	}
	
	public function publishers()
	{
		return $this->belongsToMany('App\Publisher' , null , 'research_id' , 'publisher_id');
	}

	public function categories()
	{
		return $this->belongsToMany('App\Category' , null , 'research_id' , 'category_id');
	}

	public function mainSubjects()
    {
    	return $this->belongsToMany('App\MainSubject');
    }
    public function subSubjects()
    {
    	return $this->belongsToMany('App\SubSubject');
    }
    public function getMainSubjectsAllAttribute()
    {
    	$ids = $this->subSubjects()->get()->pluck('_id');
    	$mainSubjects = $this->mainSubjects()->with(['subSubjects' => function($q) use ($ids){
    		$q->whereIn('_id' , $ids);
    	}])
    	->with('another')
    	->get();
    	return $mainSubjects;
    }

}
